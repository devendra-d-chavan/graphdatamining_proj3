#!/usr/bin/python

"""
Usage:
python test_model.py <model filepath> <test dataset filepath> <dataset name>

For example,
python test_model.py ../stanford-ner/classifiers/wikipedia-ner-model-4.train.ser.gz 
../datasets/wikipedia.test wikipedia

Generates files in the current directory:
    1. Performance metrics of testing the model: <model name>.test.perf
    2. Classified items: <model name>.test.output
"""

import os

def test(model_filepath, test_dataset_filepath, dataset_name):
    """
    Tests the model against the test dataset
    """
    import subprocess
    
    result_filepath = dataset_name + '.test.output'
    output_filepath = dataset_name + '.test.perf'

    parent_dir = os.path.dirname(os.path.dirname(                               
                                            os.path.abspath(model_filepath)))   
    with open(result_filepath, 'w') as result_file, \
            open(output_filepath, 'w') as output_file:
        print 'Evaluating model...'
        subprocess.call(['java', 
                            '-cp', 
                            os.path.join(parent_dir, 'stanford-ner.jar'),
                            'edu.stanford.nlp.ie.crf.CRFClassifier',
                            '-loadClassifier',
                            model_filepath,
                            '-testFile',
                            test_dataset_filepath],
                            stdout = result_file, 
                            stderr = output_file)

if __name__ == '__main__':
    import sys
   
    model_filepath = sys.argv[1]
    test_dataset_filepath = sys.argv[2]
    dataset_name = sys.argv[3]

    test(model_filepath, test_dataset_filepath, dataset_name)
