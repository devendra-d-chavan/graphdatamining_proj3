#!/usr/bin/python


"""
Partitions the file into k paritions after permuting the file contents (lines).
Partitioned files are generated in the same location as the input file.

Usage:
python partition.py <input filepath> <no. of partitions>

For example,
python partition.py ../datasets/wikipedia.py 10
"""
def split_file(src_filepath, k):

    # Permute the lines in the file
    import random
    import tempfile
    import os

    with open(src_filepath,'r') as src_file:        
        data = [(random.random(), line) for line in src_file]

    data.sort()

    _, temp_filepath = tempfile.mkstemp()

    with open(temp_filepath, 'w') as temp_file:
        for _, line in data:
            temp_file.write(line)

    # Partition the file into test (1 part) and training (k-1 parts)
    test_part_size = int(len(data)/k)

    for i in xrange(1, k+1):
        linecount = 1
        with open(temp_filepath, 'r') as temp_file, \
                open(src_filepath + '.test.' + str(i), 'w') as test_file, \
                open(src_filepath + '.train.' + str(i), 'w') as train_file:
            for line in temp_file:
                if linecount >= (i-1)*test_part_size and linecount < i*test_part_size:
                    test_file.write(line)
                else:   
                    train_file.write(line)
                linecount += 1

    os.remove(temp_filepath)

if __name__ == '__main__':
    import sys

    train_file = sys.argv[1]
    num_partitions = int(sys.argv[2])
    split_file(train_file, num_partitions)
