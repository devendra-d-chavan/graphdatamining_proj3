#!/usr/bin/python

"""
Usage:
python cross_validate_train.py <property file path> <no. of folds> 
    <dataset name> <dataset filepath>

For example,
python cross_validate_train.py ../stanford-ner/classifiers/wikipedia.prop 10 
wikipedia ../datasets/

Generates files:
    1. Performance result of testing the model: 
        test_output.<dataset name>.<partition no.> 
    2. Classified items: <cross_validate>.<dataset name>.<partition no.>
"""

import os

def create_property_files(src_filepath, datasets_dir, dataset_name, k):
    """
    Generates the properties files that are consumed by the stanford ner 
    applicattion
    """
    prop_filepaths = []
    model_filepaths = []
    datasets_dir = os.path.abspath(datasets_dir)
    src_filepath = os.path.abspath(src_filepath)

    # Permute the lines in the file
    for i in xrange(1, k+1):
        new_prop_filepath = src_filepath + '.' + str(i)
        prop_filepaths.append(new_prop_filepath)
        with open(src_filepath, 'r') as src_file, \
                open(new_prop_filepath, 'w') as prop_file:
            for line in src_file:
                if line.startswith('trainFile') or \
                    line.startswith('serializeTo'):
                    prop_name, prop_value = line.split('=')                    

                    if line.startswith('trainFile'):
                        prop_value = os.path.join(datasets_dir, 
                                        dataset_name + '.train.train.'+ str(i)) 
                    elif line.startswith('serializeTo'):                    
                        prop_value = os.path.join(
                                        os.path.dirname(src_filepath), 
                                        dataset_name + '-ner-model-'+ str(i) + 
                                                            '.train.ser.gz')
                        model_filepaths.append(prop_value)

                    prop_file.write('{0}={1}\n'
                                        .format(prop_name, prop_value))

                else:
                    prop_file.write(line)

    return prop_filepaths, model_filepaths

def train(prop_filepath):
    """
    Trains the model using the specified properties files that contains the 
    path to the training dataset
    """
    import subprocess
    import time
    import os.path
    
    parent_dir = os.path.dirname(os.path.dirname(
                                            os.path.abspath(prop_filepath)))
    start_time = time.time()
    subprocess.call(['java', 
                        '-cp', 
                        os.path.join(parent_dir, 'stanford-ner.jar'),
                        'edu.stanford.nlp.ie.crf.CRFClassifier',
                        '-prop',
                        prop_filepath])
    end_time = time.time()
    time_taken = end_time - start_time

    # Get the partition number from the property file name
    _, ext = os.path.splitext(prop_filepath)

    perf_filepath = 'train_perf.' + ext.strip('.')
    with open(perf_filepath, 'w') as f:
        f.write(str(time_taken) + '\n')
    
def test(args):
    """
    Tests the generated model against the remaining partition elements (1 
    test parttion of the k total partitions)
    """
    import subprocess
    
    model_filepath = args[0]
    datasets_dir = args[1]
    dataset_name = args[2]    
    
    t = model_filepath[model_filepath.rfind('-')+1:]
    partition_index = t[:t.find('.')]
    result_filepath = 'cross_validate.{0}.{1}'.format(dataset_name, 
                                                      partition_index)
    output_filepath = 'test_output.{0}.{1}'.format(dataset_name, 
                                                      partition_index)

    test_dataset_filepath = os.path.join(datasets_dir, 
                                            '{0}.train.test.{1}'
                                            .format(dataset_name, 
                                                    partition_index))

    parent_dir = os.path.dirname(os.path.dirname(
                                            os.path.abspath(model_filepath)))

    with open(result_filepath, 'w') as result_file, \
            open(output_filepath, 'w') as output_file:
        print 'Evaluating partition ' + partition_index
        subprocess.call(['java', 
                            '-cp', 
                            os.path.join(parent_dir, 'stanford-ner.jar'),
                            'edu.stanford.nlp.ie.crf.CRFClassifier',
                            '-loadClassifier',
                            model_filepath,
                            '-testFile',
                            test_dataset_filepath],
                            stdout = result_file, 
                            stderr = output_file)

if __name__ == '__main__':
    import sys
    from multiprocessing import Pool
    
    src_prop_file = sys.argv[1]
    num_partitions = int(sys.argv[2])
    dataset_name = sys.argv[3]
    datasets_dir = sys.argv[4]

    prop_filepaths, model_filepaths = create_property_files(src_prop_file, 
                                                            datasets_dir,
                                                            dataset_name, 
                                                            num_partitions)

    # Configurable to the number of cores in the host
    pool = Pool(processes = 4)

    # Train the model (k-1 partitions)
    args = prop_filepaths
    pool.map(train, prop_filepaths)

    args = [(model_filepath, datasets_dir, dataset_name) for model_filepath in 
                                                            model_filepaths]

    # Test the partition
    pool.map(test, args)
