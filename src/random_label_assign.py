#!/usr/bin/python

"""
Randomly assign 4 labels to the emma dataset
{O, PERS, ORG, LOC} to lines {1, 2, [3], [4]} and repeat (for each combination)

Generated output files with the suffix according to the labels in the dataset
(eg: emma-3_PER_LOC.train)

Usage:
python random_assign_labels.py <num labels> <input filepath> <output filepath>

For example,
python random_assign_labels.py 4 ../datasets/emma.train ../datasets/emma-4.train

"""

def assign_random_labels(num_labels, input_filepath, output_filepath):
    from itertools import combinations
    import os.path

    basename, ext = os.path.splitext(output_filepath)

    labels = ['O', 'PERS', 'LOC', 'ORG']
    for label_set in combinations(labels, num_labels):
        suffix = ''
        for element in label_set:
            suffix += '_' + element

        new_output_filepath = basename  + suffix + ext

        with open(input_filepath) as input_file, \
                open(new_output_filepath, 'w') as output_file:
            linecount = 1
            for line in input_file:
                data, label = line.split('\t')
                if linecount == 1:
                    label = label_set[0]
                elif linecount == 2:
                    label = label_set[1]
                elif linecount == 3:
                    label = label_set[2]
                elif linecount == 4:
                    label = label_set[3]
                
                linecount += 1

                if linecount > num_labels:
                    linecount = 1

                output_file.write('{0}\t{1}\n'.format(data, label))

if __name__ == '__main__':
    import sys

    num_labels = int(sys.argv[1])
    input_filepath = sys.argv[2]
    output_filepath = sys.argv[3]
    assign_random_labels(num_labels, input_filepath, output_filepath)
