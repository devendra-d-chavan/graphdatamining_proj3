#!/usr/bin/python

"""
Generates power set of the {O}, {LOC}, {PER}, {ORG} labels in the same 
directory as the input file

Usage:
python generate_power_set.py <dataset filepath>

For example,
python generate_power_set.py ../datasets/emma.train 

"""

def write_powerset_labels(labels, input_filepath):
    """
    Write the labels in the list to the output file, write {O} for the rest of the
    lists

    Args:
        labels:             List of labels that are present in the power set
        input_filepath:     Filepath of the dataset file

    Returns:
        None
    """
    import os.path

    if len(labels) == 0:
        return

    suffix = ''
    for label in labels:
        suffix = suffix + '_' + label

    base_name, ext = os.path.splitext(os.path.abspath(input_filepath))
    output_filepath = base_name + suffix + ext

    with open(input_filepath) as input_file, \
            open(output_filepath, 'w') as output_file:    
        for line in input_file:
            data, label = line.split('\t')
            new_label = 'O'
            label = label.strip()

            if label in labels:
                new_label = label

            output_file.write('{0}\t{1}\n'.format(data, new_label))

def get_labels_power_set():
    """
    Generates the power sets for the labels {O, LOC, PERS, ORG}

    Returns:
        The list of power sets
    """
    power_set = powerset('abcd')
    labels = list()
    for element in power_set:
        element_set = set()
        for item in element:
            if item == 'a':
                element_set.add('O')
            elif item == 'b':
                element_set.add('LOC')
            elif item == 'c':
                element_set.add('PERS')
            elif item == 'd':
                element_set.add('ORG')

        if len(element_set) > 0:
            labels.append(element_set)

    return labels

def powerset(iterable):
    """
    Python itertools recipe to generate a power set
    powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)
    """
    from itertools import chain, combinations
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(len(s)+1))

if __name__ == '__main__':
    import sys

    input_filepath = sys.argv[1]
    labels_power_set = get_labels_power_set()
    for labels in labels_power_set:
        write_powerset_labels(labels, input_filepath) 
