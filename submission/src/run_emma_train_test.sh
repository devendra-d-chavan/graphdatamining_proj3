#!/bin/bash
#java -cp ../stanford-ner/stanford-ner.jar  edu.stanford.nlp.ie.crf.CRFClassifier -prop ../stanford-ner/classifiers/emma.prop &>test.log
#grep '^Total time spent in*' test.log | awk -F" " '{print $6}' | sed 's/s//'
#java -cp ../stanford-ner/stanford-ner.jar edu.stanford.nlp.ie.crf.CRFClassifier -loadClassifier ../stanford-ner/classifiers/emma-ner-model.ser.gz -testFile ../datasets/emma.test &> test2.log
# 2 labels datasets
DSDIR=../datasets
LOGDIR=./log
DSL2=(emma_LOC_O  emma_PERS_LOC  emma_PERS_O emma_PERS_LOC_O emma-RAND-4_O_PER_LOC_ORG emma-RAND-3_O_LOC_ORG emma-RAND-3_O_PER_LOC emma-RAND-3_O_PER_ORG emma-RAND-3_PER_LOC_ORG emma-RAND-2_LOC_ORG emma-RAND-2_O_LOC emma-RAND-2_O_ORG emma-RAND-2_O_PER emma-RAND-2_PER_LOC emma-RAND-2_PER_ORG)
OUTPUT=emma-Q7-results.txt
PRPFILE=../stanford-ner/classifiers/emma.prop
for ((i=0;i<${#DSL2[@]}; i++)); do
     #replace train dataset in property file
     
     sed -i 's/emma/'${DSL2[i]}'/' ${PRPFILE}

     java -cp ../stanford-ner/stanford-ner.jar edu.stanford.nlp.ie.crf.CRFClassifier -prop ${PRPFILE} &> ${LOGDIR}/${DSL2[i]}-train.log

     TRAINTIME=`grep '^Total time spent in*' ${LOGDIR}/${DSL2[i]}-train.log | awk -F" " '{print $6}' | sed 's/s//'`

     echo "${DSL2[i]}" >> ${OUTPUT}
     echo "${TRAINTIME}" >> ${OUTPUT}

     java -cp ../stanford-ner/stanford-ner.jar edu.stanford.nlp.ie.crf.CRFClassifier -loadClassifier ../stanford-ner/classifiers/${DSL2[i]}-ner-model.ser.gz -testFile ../datasets/emma.test &> ${LOGDIR}/${DSL2[i]}-test.log

     tail -n 4 ${LOGDIR}/${DSL2[i]}-test.log >> ${OUTPUT}

     echo "" >> ${OUTPUT}

     #replace back
     sed -i 's/'${DSL2[i]}'/emma/' ${PRPFILE}
done
