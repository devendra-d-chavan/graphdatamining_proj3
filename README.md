CSC 591 Graph Data Mining - Project 3: Named Entity Recognition
==============================

Team 12: Xiaocheng Zou [xzou2], Kandarp Srivastava [ksrivas], Devendra D. Chavan [ddchavan]
------------------------
Overview
----------------
Named-Entity Recognition (NER) is a task in the information extraction pipeline 
that seeks to locate and classify different elements present in a text into 
predefined categories, such as the names of persons, organizations, locations, 
etc. It is one of the key steps in building Semantic Webs and constructing 
semantically aware search engines. Typically, NER tasks are accomplished by 
using either rule based methods or probabilistic methods. Hidden Markov Models 
(HMMs) and Conditional Random Fields (CRFs) are probabilistic graphical models 
commonly used to build NER models. CRF models usually offer a better 
performance than HMMs because they can take into consideration both past and 
future states. These models are typically trained using supervised learning 
methods, i.e., trained using data with known entity tags.


Goal
----------------
Build a CRF-based NER model. In order to do that, your team will need to

 1. Understand the theoretical workings of the CRF model.
 2. Analyze and understand the workings of the CRF implementation provided.
 3. Train and test a CRF model using the datasets provided.

IF NOT USING CROSS VALIDATION: 
  TWO STEPS:
   
  1.  java -cp stanford-ner.jar edu.stanford.nlp.ie.crf.CRFClassifier -prop ./classifiers/emma.prop
  First step is generating train model, the key input is the property (in this case, its austen.prop).
  There is a lot of parameters in this property file we can tune, currently we only use the default parameter listed on webpage. 
  For us, we should specify the path to the property file.
  2.  java -cp stanford-ner.jar edu.stanford.nlp.ie.crf.CRFClassifier -loadClassifier ner-model.ser.gz -testFile wikipedia.test 
  Second step is to run the model on the test model. 




Usage
---------------

  1. Partitioning the dataset for k fold cross validation
  `python partition.py ../datasets/wikipedia.train 10` 

      + The property files that are consumed by the `stanford-ner` library are 
      generated in the `stanford-ner/classifiers` directory. 
      + A property files is generated for each partition of the training dataset.


  2. Run cross validation on the partitioned dataset
  `python cross_validate_train.py ../stanford-ner/classifiers/wikipedia.prop 
  10 wikipedia ../datasets/`
    
    Generated files in current directory:

    1. `cross_validate.<dataset name>.<partition no.>`: Classified results of 
    each partition's cross validation test 
    2. `test_output.<dataset name>.<partition no.>`: Performance metrics of 
    each partition's cross validation test
    3. `train_perf.<partition no.>`: Time taken to train the model (in seconds)
    
    Generated models in `stanford-ner/classifiers` directory:
    `<dataset name>-ner-model-<partition no.>.train.ser.gz`
    
    Choose the **best** model from the generated models.


  3. Generate power sets of the *emma* dataset  
  `python generate_power_set.py ../datasets/emma.train`
    The files containing the power sets are written to the same directory as 
    the input file i.e. `../datasets/` in this example and are named with the 
    suffix containing the power set name. For example, `emma_LOC_PER.train` 
    indicates a power set containing *{LOC, PER}*


  4. Assign random labels to the *emma* dataset
  `python random_label_assign.py 4 ../datasets/emma.train 
  ../datasets/emma-4.train`
    
    An output file is generated for each combination of the label set.
    For example, for a label set of length 3, the output files will be with the
    suffix `O_PER_LOC`, `O_PER_ORG`, `O_LOC_ORG`.


  5. Testing the **best** model 
  `python test_model.py <model filepath> <test dataset filepath> <dataset name>`
    
    For example,
    `python test_model.py 
    ../stanford-ner/classifiers/wikipedia-ner-model-4.train.ser.gz 
    ../datasets/wikipedia.test wikipedia`
    
    Generated files in the current directory: 
    
    1. `<dataset name>.test.output` contains classified tokens
    2. `<dataset name>.test.perf` contains the performance metrics


